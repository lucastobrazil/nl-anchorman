-- --------------------------------------------------------
-- Host:                         new-devintranet
-- Server version:               5.1.69-0ubuntu0.10.04.1-log - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-07-02 16:04:06
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for karaoke
DROP DATABASE IF EXISTS `karaoke`;
CREATE DATABASE IF NOT EXISTS `karaoke` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `karaoke`;


-- Dumping structure for table karaoke.news
DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `articleid` int(10) NOT NULL AUTO_INCREMENT,
  `headline` text,
  `month` tinytext,
  `thumbnail` text,
  `tagline` text,
  `article` text,
  `postdate` datetime NOT NULL,
  `order_col` int(10) DEFAULT NULL,
  `for_live` tinyint(1) DEFAULT '0',
  `for_disc` tinyint(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`articleid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
