<? 
	$prefix = "http://".$_SERVER['SERVER_NAME'];
	$testingfolders = "/design_tests/anchorman/upload-mechanism/files/"; // this makes up for our subdirectories on dev for now
	// $testingfolders = "/nightlife/nl-anchorman/upload-mechanism/files/"; // this makes up for our subdirectories on dev for now
?>
<script>
	$(document).bind('dragover', function (e) {
	    var dropZone = $('#dropzone'),
	        timeout = window.dropZoneTimeout;
	    if (!timeout) {
	        dropZone.addClass('in');
	    } else {
	        clearTimeout(timeout);
	    }
	    var found = false,
	      	node = e.target;
	    do {
	        if (node === dropZone[0]) {
	       		found = true;
	       		break;
	       	}
	       	node = node.parentNode;
	    } while (node != null);
	    if (found) {
	        dropZone.addClass('hover');
	    } else {
	        dropZone.removeClass('hover');
	    }
	    window.dropZoneTimeout = setTimeout(function () {
	        window.dropZoneTimeout = null;
	        dropZone.removeClass('in hover');
	    }, 100);
	});
</script>
<form id="newsarticle" name="newsarticle" class="TTWForm" method="post" novalidate enctype="multipart/form-data">
	<h1><? if ($edit){echo "Editing";}else{ echo "Adding";} ?> News Article</h1>
	<? if ($edit){echo "<h3 id='articleid'>Article ID: <span>" . $articleid . "</span></h3>";}?>
	
	<div id="field4-container" class="field f_100" >
		<label for="field4">
			<?if(!$edit){echo "Upload ";}?>Thumbnail Graphic <span class='label label-warning' style='display: inline-block; margin-top: -5px;'>Image should be roughly 400x400 pixels (JPEG, PNG, GIF).</span>
		</label>
	   				
	  <?php  include('upload-form.php'); ?>

	</div>

			  
<div id="field3-container" class="field f_100"  >
   <label for="field3"> Month </label>
   <select name="month" id="month" required="required" >
		<option value="January" <? if ($edit && $month == "January"){echo "selected";}?>>
			 January
		</option>
		<option name="month" value="February" <? if ($edit && $month == "February"){echo "selected";}?>>
			 February
		</option>
		<option name="month" value="March" <? if ($edit && $month == "March"){echo "selected";}?>>
			 March
		</option>
		<option name="month" value="April" <? if ($edit && $month == "April"){echo "selected";}?>>
			 April
		</option>
		<option name="month"value="May" <? if ($edit && $month == "May"){echo "selected";}?>>
			 May
		</option>
		<option name="month" value="June" <? if ($edit && $month == "June"){echo "selected";}?>>
			 June
		</option>
		<option name="month" value="July" <? if ($edit && $month == "July"){echo "selected";}?>>
			 July
		</option>
		<option name="month"  value="August" <? if ($edit && $month == "August"){echo "selected";}?>>
			 August
		</option>
		<option name="month"  value="September" <? if ($edit && $month == "September"){echo "selected";}?>>
			 September
		</option>
		<option name="month"  value="October" <? if ($edit && $month == "October"){echo "selected";}?>>
			 October
		</option>
		<option name="month"  value="November" <? if ($edit && $month == "November"){echo "selected";}?>>
			 November
		</option>
		<option name="month"  value="December" <? if ($edit && $month == "December"){echo "selected";}?>>
			 December
		</option>
   </select>
</div>


<div id="field1-container" class="field f_100">
	   <label for="field1">
			Headline
	   </label>
	   <input type="text" name="headline" id="headline" class='headline' required value="<?if ($edit){echo $headline;}?>">
	   <div id="input-count">Characters Remaining: <span id="chars"></span></div>
</div>

  
  <div id="field5-container" class="field f_100">
	   <label for="field5">
			Tagline
	   </label>
	   <textarea rows="1" cols="80" name="tagline" id="tagline" required><?if ($edit){echo $tagline;}?></textarea>
	     <div id="input-count">Characters Remaining: <span id="chars2"></span></div>
  </div>
  
  
  <div id="field6-container" class="field f_100" style="display: none;">
	   <label for="field6">
			Article
	   </label>
	   <textarea  id="ckeditor" rows="50" cols="90" name="article" id="field6" required><?if ($edit){echo $article;}?></textarea>
  </div>
			  
		  
<script type="text/javascript">

$('.cancel').on('click', function(){
	$(this).hide();
	// alert('rar');
	$("#files div").remove();
	$('#dropzone').show();
	$('.label-warning').show();
	$('.fileinput-button').show();
	$("span#url").hide();
	
});


var elem = $("#chars");
$("#headline").limiter(25, elem);	

var elem = $("#chars2");
$("#tagline").limiter(215, elem);


var fileurl = "empty";
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
     var url = 'upload-mechanism/',
		uploadButton = $('<button/>')
		.addClass('btn')
		.prop('disabled', true)
		.text('Processing...')
		.on('click', function () {
		event.preventDefault();
		    var $this = $(this),
		        data = $this.data();
		    $this
		        .off('click')
		        .text('Abort')
		        .on('click', function () {
		            $this.remove();
		            data.abort();
		        });
		    data.submit().always(function () {
				$this.remove();
		    });
		});
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 5000000, // 5 MB
        loadImageMaxFileSize: 15000000, // 15MB
        // disableImageResize: false,
        previewMaxWidth: 150,
        previewMaxHeight: 150,
		disableImageResize: /Android(?!.*Chrome)|Opera/
			.test(window.navigator && navigator.userAgent),
		imageMaxWidth: 380,
		imageMaxHeight: 380,
		imageCrop: true,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
			$('.cancel').show();
			$('.label-warning').hide();
        data.context = $('<div/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .prepend($('<span/>').text());
				$('#dropzone').hide();
				$('.fileinput-button').hide();

            if (!index) {
                node
                    // .append('<br>')
                    .prepend(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append(file.error);
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
	
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .bar').css(
            'width',
            progress + '%'
        );

    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
				$('#url').html("<a href='"+file.url+"' target='_blank'>"+file.name+"</a>");
				$('#uploadconfirm').fadeIn().delay(1000).fadeOut();
				$('.cancel').hide();
				$('.disabled').removeClass('disabled');
				$('#upload-stuffs-container').append("<button type='button' id='delete-thumb' class='btn btn-danger delete' style='margin-left:20px;'><i class='icon-trash icon-white'></i><span>Delete</span></button>");
	
			// below is the default 'link' that got placed underneath the file
			// var link = $('<a>').attr('target', '_blank').prop('href', file.url);
            // $(data.context.children()[index]).wrap(link);
			
			
			var filename = file.name;
			// alert(file.name);
				// alert(fileurl);

        });
    }).on('fileuploadfail', function (e, data) {
	// alert (files);
        $.each(data.files, function (index, file) {
            var error = $('<span/>').text(file.error);
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    });
});

$(document).on('click', '#delete-thumb', function(){
	 //alert('clicked!!');
	$('#thumb-delete').fadeOut(1000, function(){
	$('#upload-stuffs-container').fadeIn(1000);
			$("span#url").hide();
	});
});

</script> 
<label>Publish To</label>
	<input type="radio" name="publish_to" value="0" <? if ($edit != 0 && $for_live == "1" && $for_disc == "0"){echo 'checked';} ?>>Live Guide Only</input>
	<input type="radio" name="publish_to" value="1" <? if ($edit != 0 && $for_disc == "1" && $for_live == "0"){echo 'checked';} ?>>Disc Only</input>
	<input type="radio" name="publish_to" value="2" <? if ($edit != 0 && $for_live == "1" && $for_disc == "1"){echo 'checked';} ?>>Both</input>
	<input type="radio" name="publish_to" value="3" <? if ($edit != 0 && $for_live == "0" && $for_disc == "0"){echo 'checked';} ?>>Neither (Draft)</input>
<label>Publish</label>
<div id="form-submit" class="field f_100 clearfix">
<!--<a href="#" class="preview btn  btn-primary disabled">Preview <? //if ($edit){echo "Changes";}?></a>-->
<? 	if ($edit){
		echo "<a href='#' class='savechanges btn btn-success' >Save Changes</a>";
		echo " <a href='#' class='submit btn btn-success' ><i class='fa fa-plus-circle'></i> Save As New...</a>";
	}else{
		echo "<a href='#' class='submit  btn btn-success disabled' >Submit</a>";
		// echo "<a href='#' class='submit button-roundy'  onclick='document.newsarticle.submit()'>Submit</a>";
		}
?>
<span class='label label-warning'>Please make sure you've uploaded the thumbnail and filled out the form before Publishing.</span>
</div>
</form>		