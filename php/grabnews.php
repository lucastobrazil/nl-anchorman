<?php
	// $debug=1;
	// ==============CONNECT ==================//
	$con = mysqli_connect("new-devintranet","root","");
	// $con = mysqli_connect("localhost","root","root");
	// Check connection
	if( mysqli_connect_errno() ){
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}

	if( !mysqli_select_db($con, "karaoke") ){
		die('could not connect to DB: `karaoke`');
	}
	$action = "";
	$action  = $_REQUEST['action'];

			if($action != "edit_newsitem"){
				// echo "<pre style='border: 1px solid blue;'>{$action}</pre>";
				//continue with loading in the news item
			}else{ 
				// echo "<pre style='border: 1px solid blue;'>not {$action}</pre>";
				$month = "";
				$headline = "";
				$tagline = "";
				$article = "";
			}

	$output = "";

	// if($debug){ echo "<pre style='border: 1px solid blue;'>"; print_r($row); echo "</pre>"; }
	switch ($action){
		case 'get_active_headlines':
			$sql = "SELECT articleid, headline, postdate, articleid FROM news WHERE for_live = 1 OR for_disc = 1";
			if($result = mysqli_query($con, $sql)){
				$rc = mysqli_num_rows($result);
				for($i = 0; $i < $rc; $i++){
					$row = mysqli_fetch_assoc($result);
					
					$articleid = $row['articleid'];
					$headline = $row['headline'];
					$postdate = $row['postdate'];
					
					$output .= "<tr class='article-list-item' id='$articleid'><td class='postdate'><span>{$postdate}</span></td><td class='headline lightgray'>";
					$output .= "<a href='#' target='_self'>{$headline}</a></td><td class='delete hoverable'>Deactivate</td/tr>";
				//	if($debug){ echo "<pre style='border: 1px solid blue;'>"; print_r($row); echo "</pre>"; }
				}
			}else{
				$output .= "no result";
			}
		break;
		case 'get_all_headlines':

			$sql = "SELECT articleid, headline, postdate, month, articleid, for_live, for_disc FROM news ORDER BY order_col asc";
			if($result = mysqli_query($con, $sql)){
				$rc = mysqli_num_rows($result);
				for($i = 0; $i < $rc; $i++){
					$row = mysqli_fetch_assoc($result);
					
					$articleid = $row['articleid'];
					$headline = $row['headline'];
					$postdate = $row['month'];
					$for_live = $row['for_live'];
					$for_disc = $row['for_disc'];
					
					if($for_live == 1 && $for_disc == 0){ //if item is for live, but not for disc
						$output .= "<tr class='article-list-item activated'id='$articleid'><td class='dragger'>||</td><td class='postdate'><span>{$postdate}</span></td><td class='headline lightgray'>";
						$output .= "<a href='#' target='_self'>{$headline}</a></td><td class='hoverable not-for-disc'><span>DISC</span></td><td class='for-live hoverable'><span>LIVE</span></td></tr>";
					}else if($for_live == 1 && $for_disc == 1){ //if item is for live, and for the disc
						$output .= "<tr class='article-list-item activated'  id='$articleid'><td class='dragger'>||</td><td class='postdate'><span>{$postdate}</span></td><td class='headline lightgray'>";
						$output .= "<a href='#' target='_self'>{$headline}</a></td><td class='hoverable for-disc'><span>DISC</span></td><td class='for-live hoverable'><span>LIVE</span></td></tr>";
					}else if($for_live == 0 && $for_disc == 0){ //if item is not for live, and not for the disc
						$output .= "<tr class='article-list-item deactivated'  id='$articleid'><td class='dragger'>||</td><td class='postdate'><span>{$postdate}</span></td><td class='headline lightgray'>";
						$output .= "<a href='#' target='_self'>{$headline}</a></td><td class='hoverable not-for-disc'><span>DISC</span></td><td class='not-for-live hoverable'><span>LIVE</span></td></tr>";
					}else{ //if item is not for live, but for the disc
						$output .= "<tr class='article-list-item activated ' id='$articleid'><td class='dragger'>||</td><td class='postdate'><span>{$postdate}</span></td><td class='headline lightgray'>";
						$output .= "<a href='#' target='_self'>{$headline}</a></td><td class='hoverable for-disc'><span>DISC</span></td><td class='not-for-live hoverable'><span>LIVE</span></td></tr>";
					}
				//	if($debug){ echo "<pre style='border: 1px solid blue;'>"; print_r($row); echo "</pre>"; }
				}
			}else{
				$output .= "no result";
			}
		break;
		case 'get_deactivated_headlines':

			$sql = "SELECT articleid, headline, postdate, month, articleid, for_live, for_disc FROM news WHERE for_live=0 and for_disc=0 ORDER BY postdate desc";
			if($result = mysqli_query($con, $sql)){
				$rc = mysqli_num_rows($result);
				for($i = 0; $i < $rc; $i++){
					$row = mysqli_fetch_assoc($result);
					
					$articleid = $row['articleid'];
					$headline = $row['headline'];
					$postdate = $row['month'];
					//$active = $row['active'];

					$output .= "<tr class='article-list-item deactivated ' id='$articleid'><td class='postdate'><span>{$postdate}</span></td><td class='headline lightgray'>";
					$output .= "{$headline}</td><td class='delete-button  hoverable'><span class='fa fa-times-circle'></span></td></tr>";
					
				//	if($debug){ echo "<pre style='border: 1px solid blue;'>"; print_r($row); echo "</pre>"; }
				}
			}else{
				$output .= "no result";
			}
		break;
		case 'get_full_article':
			$sql = "SELECT * FROM news";
			if($result = mysqli_query($con, $sql)){
				$rc = mysqli_num_rows($result);
				for($i = 0; $i < $rc; $i++){
					$row = mysqli_fetch_assoc($result);
					if($debug){ echo "<pre style='border: 1px solid blue;'>"; print_r($row); echo "</pre>"; }
					
					$headline = $row['headline'];
					$article = $row['article'];
					$output .= "<div id='article' class='main-article' ><h1>{$headline}</h1> {$article} </div><hr />";
				}
			}else{
				$output .= "no result";
			}
		break;
		case 'get_hdms_news':
			$sql = "SELECT * FROM news WHERE for_live = 1 OR for_disc = 1"; // this may need to change now we have two builds
			$year = date('F j, Y');
			if($result = mysqli_query($con, $sql)){
				$rc = mysqli_num_rows($result);
				for($i = 0; $i < $rc; $i++){
					$row = mysqli_fetch_assoc($result);
					if($debug){ echo "<pre style='border: 1px solid blue;'>"; print_r($row); echo "</pre>"; }
					
					$headline = $row['headline'];
					$article = $row['article'];
					$month = $row['month'];
					$output .= "<div id='page' ><span class='month'>{$month} {$year}</span> <h1>{$headline}</h1>{$article} </div>";
				}
			}else{
				$output .= "no result";
			}
		break;
		case 'edit_newsitem':
		// $debug=1;
		$edit = 1;
		 $articleid = $_REQUEST['id'];
		// echo $articleid;
			$sql = "SELECT headline, article, tagline, month, articleid, thumbnail, for_live, for_disc FROM news WHERE articleid = $articleid";
			
			if($result = mysqli_query($con, $sql)){
				$rc = mysqli_num_rows($result);
				for($i = 0; $i < $rc; $i++){
					$row = mysqli_fetch_assoc($result);
					 if($debug){ echo "<pre style='border: 1px solid blue;'>"; print_r($row); echo "</pre>"; }
					
					$headline = $row['headline'];
					$article = $row['article'];
					$month = $row['month'];
					$articleid = $row['articleid'];
					$tagline = $row['tagline'];
					$thumbnail = $row['thumbnail'];
					$for_live = $row['for_live'];
					$for_disc = $row['for_disc'];
					
					// $output .= " <form id='newsarticle' name='newsarticle' action='submit.php' class='TTWForm box-shadow' method='post' novalidate enctype='multipart/form-data'><h3 id='articleid'>ID: $articleid</h3><span class='month edit' contentid=$id>{$month}</span> <h1 class='edit' id='headline' >{$headline}</h1><span class='edit'  id='article'>{$article}</span></form>";
					$output .= include_once('input_form.php');
				}
			}else{
				$output .= "no result";
			}
		break;
	}
	
	echo $output;
	
	//===================
	mysqli_close($con);
?>