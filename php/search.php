<?php
	$prefix = "http://".$_SERVER['SERVER_NAME'];
	// $testingfolders = "/design_tests/anchorman/upload-mechanism/files/"; // this makes up for our subdirectories on dev for now
	$testingfolders = "/nightlife/nl-anchorman/upload-mechanism/files/"; // this makes up for our subdirectories on dev for now
	// $debug=1;
	// ==============CONNECT ==================//
	// $con = mysqli_connect("new-devintranet","root","");
	$con = mysqli_connect("localhost","root","root");
	// Check connection
	if( mysqli_connect_errno() ){
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	if( !mysqli_select_db($con, "karaoke") ){
		die('could not connect to DB: `karaoke`');
	}
	$searchterm = $_POST['searchstring'];
	
	if($debug){ echo "<pre style='border: 1px solid blue;'>"; echo $searchterm; echo "</pre>"; }

	if(isset($searchterm) && $searchterm != '') {
		$query = "SELECT * FROM news WHERE (headline LIKE '%$searchterm%') OR (tagline LIKE '%$searchterm%')";

		if($result = mysqli_query($con, $query)){
			$rc = mysqli_num_rows($result);
			if($rc > 0){
				for($i = 0; $i < $rc; $i++){
					$row = mysqli_fetch_assoc($result);
					
					$articleid = $row['articleid'];
					$headline = $row['headline'];
					$tagline = $row['tagline'];
					$postdate = $row['postdate'];
					$thumbnail = $row['thumbnail'];
					
					$output .= "<tr class='article-list-item' id='$articleid'>";
					// $output .= "<span class='postdate'>{$postdate}</span>";
					$output .= "<td class='searched-headline'>";
					$output .="<a href='#' target='_self''>{$headline}</a>";
					$output .="<span class='postdate'>{$postdate}</span>";
					$output .= "<div class='thumb'><img src='{$prefix}{$testingfolders}{$thumbnail}' style='width:50px; height:50px;' /></div>";
					$output .="<div class='tagline'>{$tagline}</div>";
					$output .="</td>";
					$output .= "</tr>";
				}
			}
			else{
				$output = "<tr>No articles found.</tr>";
			}
		}
	}
	else{
		$output = "<tr>Please type an article to search</tr>";
	}

	echo $output;

	
	//===================
	mysqli_close($con);
?>

 




