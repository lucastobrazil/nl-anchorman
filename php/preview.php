<?php $version = "1.192 [Stable]"; ?>

	<script src="js/vendor/jquery.min.js"></script>
	<script src="js/hdmsnews.main.js"></script>



	<!--<meta http-equiv="refresh" content="30">-->
	<link rel="stylesheet" href="http://hdms-live.nightlife.com.au/hdmsnews/css/hdms-style.css" />
	<link rel="stylesheet" href="http://hdms-live.nightlife.com.au/hdmsnews/css/font-style.css" />

	
<div id="wrapper">

<div id="paused" class="rounded"><h1>||</h1></div>
		<div id="title">
			<div id="header">
					<div id="title-image"><img src="images/header.png" /></div><!--
 -->		
					<div id="logo"><img src="images/logo-nm.png"  /></div>
			</div>
<? $debugmode = "Debug Mode"; ?>
			<div id="ticker-container">
				<div class="ticker">
					<p class="greeting"><? echo $debugmode; ?></p>
				</div>
				
				<div class="ticker">
					<p class="datetime">Today it's <? echo $debugmode; ?></p>
				</div>
				
				<div class="ticker">
					<p class="currenttime"><? echo $debugmode; ?>PM</p>
				</div>

			</div>
			<!--<h1>Nightlife News</h1>-->

		</div>
	<div id="pages">
			<?php include('preview-hdms-news.php'); ?>	
	</div>
		

	<div id="instructions">

		<p>[Previous]<span class="key left">&laquo;</span><span class="key right">&raquo;</span>[Next]<span class="key space"> Space</span>[<span id="status">Pause</span>]<span class="key esc">ESC</span>[Return To HDMS]</p>
		<img src="images/social-graphics.png" class="social" />
	</div>
		<div class="detect-online">SOURCE: DISC // V:<?php echo $version; ?> </div>	
	</div>
	
		
		

	
	<script type="text/javascript">

		
		var paused = 0;
	

		 "use strict";

		 // DETECT ONLINE
				var arr = document.URL;
				// alert (arr);
				var tempArray = arr.split('.');
				var temp = '';
				for(var i = 0; i < tempArray.length; i++) {
					if(tempArray[i] == "php") temp = ('Detected file extension of PHP; assume online.');
				}
				// arr = temp.substring(0);
				 if(temp) {	$(".detect-online").html("SOURCE: LIVE" + " // V<?php echo $version; ?> || DEV")}else{
					$(".detect-online").html("SOURCE: DISC" + " // V<?php echo $version; ?> || DEV")};

			// SET AND DISPLAY TIME
				var now = new Date();
				var hrs = now.getHours();
				var msg = "";

				if (hrs >=  0) msg = "Mornin' Sunshine!"; // REALLY early
				if (hrs >  6) msg = "Good Morning.";      // After 6am
				if (hrs > 12) msg = "Good Afternoon!";    // After 12pm
				if (hrs > 17) msg = "Good Evening.";      // After 5pm
				if (hrs > 22) msg = "Go to bed!";        // After 10pm
					
				$("#title p.greeting").html("" + msg);
			
					
					var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
					var date = new Date();
					var hours = date.getHours();
					var minutes = date.getSeconds();
					var day = date.getDate();
					var month = date.getMonth();
					var yy = date.getYear();
					var year = (yy < 1000) ? yy + 1900 : yy;
	
				 $("#title p.datetime").html("Today it's " + months[month] + " " + day + ", " + year + ".");
			
			
			setInterval(function() {
				"use strict";
				//this makes the time auto update		
					var d = new Date();
					var hh = d.getHours();
					var m = d.getMinutes();
					var s = d.getSeconds();
					var dd = "AM";
					var h = hh;
					if (h >= 12) {
						h = hh-12;
						dd = "PM";
					}
					if (h == 0) {
						h = 12;
					}
					m = m<10?"0"+m:m;

					s = s<10?"0"+s:s;

					/* if you want 2 digit hours:
					h = h<10?"0"+h:h; */

					var pattern = new RegExp("0?"+hh+":"+m+":"+s);

					var repalcement = h+":"+m+":"+s;
					/* if you want to add seconds
					repalcement += ":"+s;  */
					repalcement += ""+dd;    
				// alert(repalcement);

					$("#title p.currenttime").html(repalcement); //update time display
					// $("#title p.currenttime").html(hours + ":" + minutes) ; //update time display
					}, 1000);
					
					
		 
		 
		 
			$(document).keydown(function(e){
				if (e.keyCode == 37) { 
					//LEFT KEY PRESSED
					$('#pages').cycle('prev');
					$('#pages').cycle('pause');
					$('.left').addClass('redbg');
					 setTimeout(function() {
						$('.left').removeClass('redbg');
					}, 50);
					
					$('#status').text("Resume");
					paused = 1;
				}else if (e.keyCode ==39){
		//RIGHT KEY PRESSED
					$('#pages').cycle('next');
					$('#pages').cycle('pause');
					$('.right').addClass('redbg');
					 setTimeout(function() {
						$('.right').removeClass('redbg');
					}, 50);
				
					$('#status').text("Resume");
					paused = 1;
					// throw 1;
				}
				else if (e.keyCode ==32){ 
		//SPACE BAR PRESSED
					if (paused == 0){$('#pages').cycle('pause'); 
					paused = 1; 
						$('.space').addClass('redbg');
						 setTimeout(function() {
							$('.space').removeClass('redbg');
						}, 50);
				
					$('#paused h1').text("||"); 
					$('#status').text("Resume");
				}
		//SPACE BAR PRESSED AGAIN, TOGGLE			
				else if (paused == 1){
					$('#pages').cycle({
						speed: '100',
						timeout: '200'
					});  
					paused = 0; 
					$('.space').addClass('redbg');
						setTimeout(function() {
							$('.space').removeClass('redbg');
						}, 50);
					$('#paused h1').text(">");
					$('#status').text("Pause"); 
				}
				

				}
				else if (e.keyCode == '27') {
		//ESCAPE KEY PRESSED
					$('#preview-container').fadeOut(1000);
				}	
			});


		$(window).load(function(){
		"use strict";
		
		if (window.CollectGarbage){
			window.CollectGarbage();
		}


		// added this back on for debug
			$('#ticker-container').cycle({
				fx: 'scrollVert',
				timeout: '10000',
				width: '800'
			});
		});				

		window.onerror = function(){
		//alert("error!");
			window.location.reload();
			return true;
		};
				</script>
		


