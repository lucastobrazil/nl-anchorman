

<style>

	div.body-wrapper{margin: 20px;}
	.deleting td{font-size: 16px !important;}
	td.delete-button{
		background-color: red;
		color: white;
		padding: 10px;	
	}
	.deleting #headline-list{  max-width: 760px; height: 460px;overflow: scroll !important;}
	.deleting #headline-list tr{padding: 1px 5px !important;}
	.deleting #headline-list td.headline{width: 600px;}
</style>
	
	
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<div class='body-wrapper deleting'>
	<span id="close-button" class="btn btn-info" style="float:right;">BACK</span>
	<h1><i class="fa fa-trash-o"></i> Delete News Items</h1>
	<div id="clear" style="clear:both;"></div>
	<p>Displaying only deactivated entries:</p>
	<div id="clear" style="clear:both;"></div>
	<div id="headline-list" class="scroll">

	</div>

</div>

<script>
$.get('php/grabnews.php', {action: 'get_deactivated_headlines'}, function(data){
		$('.deleting #headline-list').html(data); 	
});

$(document).on('click', 'td.delete-button', function() { // if it's deactivated, clicking will ACTIVATE it
	var parent = $(this).closest('tr');
	if(window.confirm(this.title || 'Delete this record?')){
		var currentx = $(this);
		$.ajax({
			type: 'get',
			url: 'php/delete.php', 
			data: 'ajax=1&id=' + parent.attr('id'),

			success: function() {
			//alert('success!');
			parent.remove();
				//parent.fadeOut();
			}
		});        
	}
});
</script>
