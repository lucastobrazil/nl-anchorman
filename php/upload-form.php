<?
if($edit){ // if we are in edit mode, first display the current thumbnail and the delete button.
	$uploadform = "<div id='thumb-delete'>";
	$uploadform .= "<img src='{$prefix}{$testingfolders}{$thumbnail}' style='width:120px; height:120px;' />";
	$uploadform .="<button type='button' id='delete-thumb' class='btn btn-danger delete' style='margin-left:20px;'><i class='icon-trash icon-white'></i><span>Delete</span></button>";
	$uploadform .= "</div>";
	$uploadform .= "<div id='upload-stuffs-container' style='display:none'>"; //create the Upload form, but hide it by default

	
}else{ //if it's a new article, show the upload form by default
	$uploadform = "<div id='upload-stuffs-container' >"; // create the Upload form, and show it
}

//upload form code below. This is always generated, but the above if,else chooses whether or not to display it
	$uploadform .= "<div id='dropzone' class='fade well'>Drop file here, or";
	$uploadform .= "<span class='btn btn-success fileinput-button'>";
	$uploadform .= "<span><i class='glyphicon glyphicon-plus-sign'></i> Browse...</span>";
//The file input field used as target for the file upload widget
	$uploadform .= "<input id='fileupload' type='file' name='files[]' multiple>";
	$uploadform .= "</span></div>";		
	$uploadform .= "<div id='progress' class='progress progress-success progress-striped'>";
	$uploadform .= "<div class='bar'></div>";
	$uploadform .= "</div>";
	$uploadform .= "<span id='uploadconfirm'>Upload Complete</span><span id='url'>";
	
	if ($edit){
		$uploadform .= $thumbnail;
	}

	$uploadform .= "</span>";	
	$uploadform .= "<div id='files'><span class='btn btn-warning cancel' ><span>Cancel</span></span></div>";

	$uploadform .= "</div>"; // close the upload-stuffs div.

	echo $uploadform; //render out the results
				
				
?>

 