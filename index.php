
<?php 
	$edit = 0; //default mode is add new article 
?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Anchorman CMS</title>

	<script src="js/vendor/jquery.min.js"></script>
    	<script src="js/vendor/jquery-ui.min.js"></script>
	<script src="js/vendor/jquery.confirm.js" type="text/javascript"></script>
		<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="js/vendor/load-image.min.js"></script>
		<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="js/vendor/canvas-to-blob.min.js"></script>
		<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="js/vendor/jquery.iframe-transport.js"></script>
	<script src="js/vendor/jquery.fileupload.js"></script>
	<script src="js/vendor/jquery.fileupload-process.js"></script>
	<script src="js/vendor/jquery.fileupload-resize.js"></script>
	<script src="js/vendor/jquery.fileupload-validate.js"></script>
	<script src="js/vendor/tablednd.js" type="text/javascript"></script>
	<script src="js/vendor/bootstrap.min.js" type="text/javascript"></script>
	
	<script src="js/nightlife.anchorman.js" type="text/javascript" ></script>

	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<!--<link href="fckeditor/sample.css" rel="stylesheet" type="text/css" />-->
	<link rel="stylesheet" type="text/css" href="css/nightlife.anchorman.css" />
	<link rel="stylesheet" type="text/css" href="css/nm-icons.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery.fileupload-ui.css" />
</head>
<body>

<div id="header">
	<img src="images/nm-main-logo.png"/>
	<h1>
		<a href="" target="_self">Anchorman - LiveGuide CMS</a> 
		<i class="fa fa-fire"></i>
	</h1>
	<span class='button-group'>
		<a target="_blank" id="build-all" >
			<span class="btn btn-danger"><i class='fa fa-cloud-upload' /></i> Publish All...</span>
		</a>
		<a target="_blank" id="build-disc" >
			<span class="btn btn-warning"><i class='fa fa-upload' /></i> Publish Disc...</span>
		</a>
	</span>

	<span class='button-group'>
		<a target="_blank" href="http://lucas.new-devintranet.nightlife.com.au/design_tests/hdmsnews/index.php?source=live" >
			<span class="btn btn-info"><i class='fa fa-flash' /></i> R&D Animated LiveGuide {DEV}</span>
		</a>
		<a target="_blank" href="http://hdms-live.nightlife.com.au/hdmsnews/index.htm" >
			<span class="btn btn-warning"><i class='fa fa-flash' /></i> ACTUAL LIVE NEWS</span>
		</a>
	</span>
</div>
<div id="preview-container" class="fullscreen-cover">
	<div id="preview-window" class="box-shadow">
			<span id="close-button" >CLOSE</span>
	</div>
</div>


<div id="headline-list-container">
	<h1>Current Articles:</h1>
	<p>
		<!-- Button trigger modal -->
		<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#searchModal">
		  Search <i class="fa fa-search"></i>
		</button>

		<a target="_self" href="?edit=0">
			<span class="btn btn-success" id="add">
				<i class="fa fa-plus-circle"></i> Add Article
			</span>
		</a>
		<button type='button' class='btn btn-danger delete-articles'>
			<i class="fa fa-trash-o"></i><span> Delete</span>
		</button>
		<button type='button' class='btn btn-warning show-hide-deactivated'>
			<i class="fa fa-arrow-down"></i><span> Show Deactivated</span>
		</button>
	</p>


		<table id="headline-list">

		</table>
		

<script>

var confirmbuildall = "<div style='padding: 20px;'><h1>Confirm Build Both Disc and Live Versions?</h1><p style='clear:both;'><a href='/design_tests/hdmsnewsbuilder/build.php' target='_blank' id='confirm-build'>	<span class='btn btn-success'>YES</span></a><a target='_blank' id='cancel-build'><span class='btn btn-danger'>NO</span></a></p></div>";

var confirmbuilddisc = "<div style='padding: 20px;'><h1>Confirm Build Disc Version only?</h1><p style='clear:both;'><a href='/design_tests/hdmsnewsbuilder/build.php?skip_live=1' target='_blank' id='confirm-build'>	<span class='btn btn-success'>YES</span></a><a target='_blank' id='cancel-build'><span class='btn btn-danger'>NO</span></a></p></div>";

$("#build-all").on('click', function(){
	$('#preview-window').html(confirmbuildall);
	$('#preview-container').fadeIn(500);
});

$("#build-disc").on('click', function(){
	$('#preview-window').html(confirmbuilddisc);
	$('#preview-container').fadeIn(500);
});

$(document)
	.on('click', '#cancel-build', function(){// fades out confirmation dialog
	 	$('#preview-container').fadeOut(500);
		$('#preview-window').html("");
	})
	.on('click', '#confirm-build', function(){ //opens popup and fades out confirmation dialog
		$('#preview-container').fadeOut(500);
		$('#preview-window').html("");
});

var toggled = 1;

	$(".show-hide-deactivated").on("click", function(){
		if(toggled == 0){
		$("tr.deactivated").hide();
			$(this).children("span").html(" Show Deactivated");
			$(this).children("i").removeClass("fa-arrow-up");
			$(this).children("i").addClass("fa-arrow-down");
		toggled = 1;
		}else{
		$("tr.deactivated").show();
			$(this).children("span").html(" Hide Deactivated");
					$(this).children("i").removeClass("fa-arrow-down");
					$(this).children("i").addClass("fa-arrow-up");
			toggled = 0;
		}
	});
</script>	
	
</div>



<div id="content-container">
	<div class="TTWForm-container" >
		<?php include_once('php/input_form.php'); ?>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="searchModalLabel">Search articles</h4>
      </div>
      <div class="modal-body">
        <input type="text" value="" placeholder="Search for an article..." id="searchinput" />
    	<div id="searchresults">
    	</div>
      </div>

    </div>
  </div>
</div>

	<script>

	$(document).ready(function(){
		var delay = (function(){
		  var timer = 0;
		  return function(callback, ms){
		    clearTimeout (timer);
		    timer = setTimeout(callback, ms);
		  };
		})();
		
		function setHeadlineClickListeners(){
			$('.searched-headline a').on('click', function(){
				// when user clicks a headline, load it up for editing
				var parent = $(this).closest('tr'); //get parent table row (for article id)
				var articleid = parent.attr('id');
				$.ajax({
					type: 'get',
					url: 'php/grabnews.php',
					data: 'action=edit_newsitem&id=' + articleid,
					beforeSend: function(){
						$("#updated").fadeIn(200);
					},
					success: function(data){
						$("#updated").delay(500).fadeOut(200);
						$('.TTWForm-container').html(data);
						$('#searchModal').modal('hide');
					}
				});
			});
		}

		$('#searchModal').on('hidden.bs.modal', function (e) {
		  // do something...
		  // $('#searchinput').val('');
		  // $('#searchresults').html('');
		})

		$('#searchinput').on('keyup', function(){
			searchstring = $(this).val();
			delay(function(){
				console.log(searchstring);
				$.ajax({
					method: "POST",
				 	url: "php/search.php",
				 	data: {searchstring: searchstring},
				 	beforeSend: function(){
				 	$('#searchresults').html('');
				 	},
				 	cache: false
				})
				.done(function( html ) {
					$( "#searchresults" ).html("<p style='color: lightgray'>Results:</p><table>" +  html + "</table>");
					setHeadlineClickListeners();
				});
		    }, 500 );
		});

	});

	$(document).on('click', '#close-button', function(){
		$('#preview-container').fadeOut();
		//populate();
	});


	$(document).on('click', '#files .btn', function(){
		$('.progress').show();
	});

	$(document).on('click', '.delete-articles', function(){
		//alert('clicked');
			$.get ('php/delete-entries.php', function(data){
						  $('#preview-window').html(data);
						  $('#preview-container').fadeIn(500);
			});
	});



	$(document).on("click", ".savechanges", function() {
	var articleid = $('#articleid span').text();
		var headline = $('input.headline').val();
		var tagline = $('#tagline').val();
		var month = $('#month').val();
		var article = "";
		var thumb = $('#url').text();
		
		var radio_value = $("input:radio[name ='publish_to']:checked").val();
		var for_disc;
		var for_live;
		
		if(radio_value == 0){
			for_disc = "0";
			for_live ="1";
		}else if (radio_value == 1){
			for_disc = "1";
			for_live ="0";
		}else if (radio_value == 2){
			for_disc = "1";
			for_live ="1";
		}else{
			for_disc = "0";
			for_live ="0";
		}
			event.preventDefault();
			$.post ('php/save.php',{headline: headline, tagline: tagline, month: month, article:article, thumb: thumb, articleid: articleid, for_live: for_live, for_disc: for_disc}, function(data){
				$('#updated').fadeIn(function(){
					setTimeout(function() { //change text to 'done' after 1500ms
						$("#updated p").html('Done!');
					}, 1500);
				}).delay(2000).queue(function(){ //reload the page
					location.reload();
				});
				
			});
	});


	$(document).on("click", ".submit", function(){
		
		var headline = $('input.headline').val();
		var tagline = $('#tagline').val();
		var month = $('#month').val();
		//var article = CKEDITOR.instances.ckeditor.getData(); 
		var article = ""; //Blank now because we don't actually author article data
		var thumb = $('#url').text();
		var radio_value = $("input:radio[name ='publish_to']:checked").val();
		var for_disc;
		var for_live;
		
		if(radio_value == 0){
			for_disc = "0";
			for_live ="1";
		}else if (radio_value == 1){
			for_disc = "1";
			for_live ="0";
		}else if (radio_value == 2){
			for_disc = "1";
			for_live ="1";
		}else{
			for_disc = "0";
			for_live ="0";
		}

	  /* stop form from submitting normally */
		event.preventDefault();
		// $('#newsarticle').submit();
		$.post("php/submit.php", {headline: headline, tagline: tagline, month: month, article:article, thumb: thumb, for_disc:for_disc, for_live:for_live}, function(data){
			$('#wrapper').html(data);
			$('#updated div.notification p').html("Submitting...");
			$('#updated').fadeIn(function(){
				setTimeout(function() { //change text to 'done' after 1500ms
					$("#updated div.notification p").html('Done!');
				}, 1500);
			}).delay(2000).queue(function(){ //reload the page
				location.reload();
			});
			
		});
	});
	</script>
	<div id="updated" class="fullscreen-cover">
		<div class="notification">
			<p>Please wait...</p>
			<img src="images/spinner-dark.gif">
		</div>
	</div>
</body>
</html>