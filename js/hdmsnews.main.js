"use strict";

var $id = function(e) {
	return document.getElementById(e);
};


/// websocket stuff ///
var system = '';
var htmlE = function(s) {
	return s.replace(/&/g,'&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
};
var ws = {
	_ws: null,
	state: 0, // 0=disconnected, 1=connecting, 2=connected, 3=subscribed
	_connectTimer: null,
	con: function() {
		ws.state = 1;
		ws._ws = new SockJS("http://127.0.0.1:9090/hl", undefined, {protocols_whitelist:["jsonp-polling"]});
		ws._ws.onopen = ws.opened; 
		ws._ws.onclose = ws.closed; 
		ws._ws.onmessage = ws.recv; 
		// onerror -> what to do?
		
		ws._connectTimer = setTimeout(function(){
			if(ws._ws && ws._ws.close) ws._ws.close();
			ws.con();
		}, 2000);
	},
	
	opened: function() {
		ws.state = 2;
		clearTimeout(ws._connectTimer);
		ws.subscribe();
	},
	closed: function() {
		if(ws.state < 2) return; // nothing actually closed if we haven't even connected
		$id('ws-output').style.display = 'none';
		ws.state = 0;
		ws.con();
	},
	
	subscribe: function() {
		if(!system) { // can't subscribe if we don't know the system code
			ws.send('control', 'who_are_you');
			return;
		}
		ws.send('control', 'subscribe', '{"systems":[{"system":"'+system+'","zone":1,"verbs":{"update_zone":["set_playstate"]}}]}');
		//ws.send('control', 'subscribe', '{"systems":[{"system":"'+system+'","zone":1}]}');
		ws.send('request_zone', 'playstate');
		ws.state = 3;
	},
	
	recv: function(msg) {
		try {
			var o = eval('('+msg.data+')');
			if((o.verb.event_category === 'response_zone' && o.verb.event === 'playstate' )  || (o.verb.event_category === 'update_zone' && o.verb.event === 'set_playstate')) {
				// update playstate
				var ps = o.object.playstate;
				$id('ws-output').style.display = '';
				$id('song').innerHTML = htmlE(ps.title);
				$id('artist').innerHTML = htmlE(ps.artist.toLowerCase());
				$id('album-artwork').innerHTML = '<img src="' + albumart_url + ps.filename + '.JPG" />'; // .replace(/T(\d)$/i, "T0$1")
			}
			else if(o.verb.event_category === 'control' && o.verb.event === 'who_i_am') {
				system = o.object.system;
				if(!ws.state != 3)
					ws.subscribe();
			}
			// ignore other messages, eg subscription ok
		} catch(e) {} // ignore parse errors
	},
	
	send: function(evcat, ev, obj) {
		ws._ws.send('{"published":{},"actor":{"type":"client","auth_user":"guest","auth_pass":"guest"},"verb":{"event_category":"'+evcat+'","event":"'+ev+'"},"object":'+(obj||'{}')+',"target":{"system":"'+system+'","zone":1}}');
	}
};



/// timer update ///
var pad2 = function(i) {
	return (i<10 ? '0':'') + i;
};
var updateTime = function() {
	var now = new Date();
	
	var hrs = now.getHours();
	var          msg = "Mornin' Sunshine!";  // REALLY early
	if(hrs >  6) msg = "Good Morning.";      // After 6am
	if(hrs > 12) msg = "Good Afternoon!";    // After 12pm
	if(hrs > 17) msg = "Good Evening.";      // After 5pm
	if(hrs > 22) msg = "Go to bed!";         // After 10pm
	$id('greeting').innerHTML = msg;
	
	var month = ['January','February','March','April','May','June','July','August','September','October','November','December'][now.getMonth()];
	var year = now.getYear();
	if(year < 1900) year += 1900;
	$id('datetime').innerHTML = "Today it's " + month + " " + now.getDate() + ", " + year;
	
	$id('currenttime').innerHTML =
		pad2( (hrs%12)||12 ) + ':' +
		pad2(now.getMinutes()) + ':' +
		pad2(now.getSeconds()) +
		(hrs >= 12 ? 'P' : 'A') + 'M';
	
};
setInterval(updateTime, 1000);


window.onload = function() {
	updateTime();
	
	/// cycle control ///
	var setOpacity = function(e, alpha) {
		e.style.opacity = alpha/100;
		if(alpha == 100) {
			e.style.filter = "";
			if(e.style.removeAttribute) {
				e.style.removeAttribute('filter');
			}
		} else {
			e.style.filter = "alpha(opacity="+alpha+")";
		}
		e.style.zoom = 1; // force layout for IE
		e.opacity = alpha;
		
		e.style.display = (alpha == 0 ? 'none' : '');
	};
	
	var pages = $id('pages').children;
	var curPage = 0;
	/*
	var faders = new Array(pages.length); // ensures we don't try to overwrite fading of a page
	var fadePage = function(pageNo, step) {
		if(faders[pageNo]) clearInterval(faders[pageNo]);
		// get current opacity
		var start = pages[pageNo].opacity || (step < 0 ? 100 : 0);
		faders[pageNo] = setInterval(function() {
			start += step;
			setOpacity(pages[pageNo], start);
			
			if(step < 0 ? start<=0 : start>=100) { // target reached
				clearInterval(faders[pageNo]);
				faders[pageNo] = null;
			}
		}, 25);
	};
	*/
	var fadeInt, clearPrev;
	var switchPage = function(p1, p2) {
		if(fadeInt) clearPrev();
		var pos = 0;
		fadeInt = setInterval(function() {
			pos += 8; // animation step
			setOpacity(pages[p1], 100-pos);
			setOpacity(pages[p2], pos);
			if(pos >= 100) clearPrev();
		}, 25);
		clearPrev = function() {
			setOpacity(pages[p1], 0);
			setOpacity(pages[p2], 100);
			clearInterval(fadeInt);
			fadeInt = null;
		};
	};
	var cycleNext = function() {
		var nextPage = curPage + 1;
		if(nextPage >= pages.length) nextPage = 0;
		switchPage(curPage, nextPage);
		curPage = nextPage;
	};
	var cyclePrev = function() {
		var nextPage = curPage - 1;
		if(nextPage < 0) nextPage = pages.length-1;
		switchPage(curPage, nextPage);
		curPage = nextPage;
	};
	var cycle;
	var startCycle = function() {
		if(cycle) return;
		cycle = setInterval(cycleNext, 12000);
		$id('status').innerHTML = 'Pause';
	};
	var stopCycle = function() {
		clearInterval(cycle);
		cycle = null;
		$id('status').innerHTML = 'Resume';
	};

	var flash = function(e) {
		e.className += " redbg";
		setTimeout(function() {
			e.className = e.className.replace(/(^| )redbg($| )/g, "$1$2");
		}, 100);
	};
	document.body.onkeydown = function(e) {
		e = e || window.event; // IE fix
		switch(e.keyCode) {
			case 37: // left
				cyclePrev();
				stopCycle();
				flash($id('left'));
			break;
			case 39: // right
				cycleNext();
				stopCycle();
				flash($id('right'));
			break;
			case 32: // space
				//var p = $id('paused').children[0];
				if(cycle) {
					stopCycle();
					//p.innerHTML = '||';
				} else {
					startCycle();
					//p.innerHTML = '&gt;';
				}
				flash($id('space'));
			break;
			case 27: // esc
				window.close();
			break;
			case 116: // F5
				return true;
			// try to block some Ctrl functions
			/*case 70: // f (find)
			case 79: // o (open)
			case 80: // p (print)
				if(e.ctrlKey && navigator.appName == "Microsoft Internet Explorer")
					window.close();*/
		}
		if(e.preventDefault) e.preventDefault();
		if(e.stopPropagation) e.stopPropagation();
		e.cancelBubble = true;
		e.returnValue = false;
		return false;
	};
	document.body.onkeyup = function(e) {
		e = e || window.event; // IE fix
		return false;
	};
	
	// hide all pages except 1st
	for(var i=0; i<pages.length; i++) {
		setOpacity(pages[i], i?0:100);
		pages[i].style.position = 'absolute';
		pages[i].style.left = 0;
		pages[i].style.top = 0;
	}
	$id('pages').style.position='relative';
	
	
	
	startCycle();
	
	
	
	/// start ticker too ///
	var tickers = $id('ticker-container').children;
	var tickerHeight = $id('ticker-container').offsetHeight;
	for(var i=0; i<tickers.length; i++) {
		var ticker = tickers[i];
		ticker.style.position = 'absolute';
		ticker.style.left = 0;
		ticker.style.top = (i ? tickerHeight:0) + 'px';
		//ticker.style.display = (i ? 'none':'');
	}
	$id('ticker-container').style.position='relative';
	
	var slideDownTo = function(e, dest) {
		var t = setInterval(function() {
			var s = parseInt(e.style.top);
			if(s < dest)
				e.style.top = Math.min(dest, s+1) + 'px';
			else
				clearInterval(t);
		}, 25);
	};
	var currentTick = 0;
	setInterval(function() {
		// slide out current
		slideDownTo(tickers[currentTick], tickerHeight);
		// slide in next
		currentTick++;
		if(currentTick >= tickers.length) currentTick = 0;
		tickers[currentTick].style.top = (-tickerHeight) + 'px';
		slideDownTo(tickers[currentTick], 0);
		
	}, 10000);
	
	
	// connect websocket
	ws.con();
	
	
	
	// auto shutdown after a while
	setTimeout(function() {
		window.close();
	}, 60000);
};


window.onerror = function(){
	//alert("Caught error!  Press OK to reload page.  (remove this alert in release version)");
	//window.location.reload();
	return true;
};