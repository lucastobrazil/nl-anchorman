/* nightlife.anchorman.js */

function check_if_activated(parent){
		//if an 'active' TD exists within the row
		if(parent.find('td.for-live').length > 0 || parent.find('td.for-disc').length > 0) {  
			parent.addClass('activated');
			parent.removeClass('deactivated');
		} else { 
			parent.addClass('deactivated');
			parent.removeClass('activated');
		}
	}

	function populate() {
		$.get('php/grabnews.php', {action: 'get_all_headlines'}, function(data){
			$('#headline-list').html(data); //display list of active articles
			$("tr.deactivated").hide();
			
			/* 
				Now that headlines are loaded, we can apply events to happen 
				when clicked. They need to appear here as dynamically loaded 
				content won't have the jQuery applied until it's loaded. 
			*/
			
		//TURN THINGS ON
		// if it's not for live, clicking will make it for live
		$(document).on('click touch', 'td.not-for-live', function() { 
			var parent = $(this).closest('tr');
			var currentx = $(this);
			
				$.ajax({
					type: 'get',
					url: 'php/toggleforlive.php',
					data: 'ajax=1&action=1&id=' + parent.attr('id'),

					success: function() {
						currentx.replaceWith('<td class="for-live hoverable"><span>LIVE</span></td>');
					}
				});        
		});

		//TURN THINGS OFF
		// if it's for live, clicking will make it not for live
			$(document).on('click touch', 'td.for-live', function() { 
					var currentx = $(this);
					var parent = $(this).closest('tr');
					$.ajax({
						type: 'get',
						url: 'php/toggleforlive.php',
						data: 'ajax=1&action=0&id=' + parent.attr('id'),

						success: function() {
							currentx.replaceWith('<td class="hoverable not-for-live"><span>LIVE</span></td>');
							check_if_activated(parent);
							
						}
					});  
			
			});
		//TURN THINGS ON	
		// if article is not for disc, clicking this will put it on the disc
			$(document).on('click touch', 'td.not-for-disc', function() { 
					var currentx = $(this);
					var parent = $(this).closest('tr');
					$.ajax({
						type: 'get',
						url: 'php/toggledisc.php',
						data: 'ajax=1&action=1&id=' + parent.attr('id'),

						success: function() {
							currentx.replaceWith('<td class="hoverable for-disc"><span>DISC</span></td>');
							check_if_activated(parent);
						}
					});  
			});
			//TURN THINGS OFF
			//if article is on the disc, clicking this will take it off the disc
			$(document).on('click touch', 'td.for-disc', function() { 
					var currentx = $(this);
					var parent = $(this).closest('tr');
					$.ajax({
						type: 'get',
						url: 'php/toggledisc.php',
						data: 'ajax=1&action=0&id=' + parent.attr('id'),

						success: function() {
							currentx.replaceWith('<td class="hoverable not-for-disc"><span>DISC</span></td>');
							check_if_activated(parent);
						}
					});  
			});			
			// when user clicks a headline, load it up for editing
			$('.headline a').click(function(){
				var parent = $(this).closest('tr'); //get parent table row (for article id)
				var articleid = parent.attr('id');
					$.ajax({
						type: 'get',
						url: 'php/grabnews.php',
						data: 'action=edit_newsitem&id=' + articleid,
						beforeSend: function(){
							$("#updated").fadeIn(200);
						},
						success: function(data){
							$("#updated").delay(500).fadeOut(200);
							$('.TTWForm-container').html(data);
							 
						}
					});	
			});

			$('#headline-list').tableDnD({
				onDragClass: "dragging",
				onDrop: function(table, row) {
					$(row).find('td.dragger').html("<span class='changing'><img src='images/spinner-dark.gif' /></span>");
					$.post('php/sorttable.php', $.tableDnD.serialize(), function(data){
						$("#headline-list-container").append(data);

						$('.changing').delay(1000).fadeOut(function(){
							$(row).find('td.dragger').html("||");
						});		
					});
				},
				dragHandle: ".dragger"
			});	
		});
	}

	$(document).ready(function(){
		populate();	    
	});
		
	//BELOW FUNCTION IS FOR CHARACTER COUNT ON THE INPUT FIELDS
	(function($) {
	    $.fn.extend( {
	        limiter: function(limit, elem) {
		        $(this).on("keyup focus", function() {
		            setCount(this, elem);
		        });
		        
				function setCount(src, elem) {
		            var chars = src.value.length;
		            if (chars > limit) {
					
		                src.value = src.value.substr(0, limit);
		                chars = limit;
						elem.parent().addClass('red');
		            }
					else{elem.parent().removeClass('red');}
		            elem.html( limit - chars );
			
		        }
		        setCount($(this)[0], elem);
	        }
	    });
	})(jQuery);
		
	$(window).load(function(){
		$(document).keydown(function(e){
		 if (e.keyCode == '27') {
				// down arrow
				$('#preview-container').fadeOut();
			}	
		});
	});
